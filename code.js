console.log('Hola mundo!');

// 2.1 Inserta dinamicamente en un html un div vacio con javascript.
const unElemento = document.createElement('div');
document.body.appendChild(unElemento);

// 2.2 Inserta dinamicamente en un html un div que contenga una p con javascript.
const unDivConP = document.createElement('div');
const unaPe = document.createElement('p');
unDivConP.appendChild(unaPe);
document.body.appendChild(unDivConP);

// 2.3 Inserta dinamicamente en un html un div que contenga 6 p utilizando un loop con javascript.
const divDeSeisPes = document.createElement('div');
document.body.appendChild(divDeSeisPes);

for (i=0; i<6; i++){
   const seisPes = document.createElement('p');
   divDeSeisPes.appendChild(seisPes);
}

// 2.4 Inserta dinamicamente con javascript en un html una p con el texto 'Soy dinámico!'.
const soyDin = document.createElement('p');
soyDin.textContent = "Soy Dinámico";
document.body.appendChild(soyDin);


// 2.5 Inserta en el h2 con la clase .fn-insert-here el texto 'Wubba Lubba dub dub'.
const element = document.querySelector('.fn-insert-here');
element.textContent = "Wubba Lubba dub dub";

// 2.6 Basandote en el siguiente array crea una lista ul > li con los textos del array.
const apps = ['Facebook', 'Netflix', 'Instagram', 'Snapchat', 'Twitter'];
const lista = document.createElement('ul');

for (const app of apps){
   var textoLista = document.createElement('li');
   textoLista.textContent = app;
   lista.appendChild(textoLista);
}
document.body.appendChild(lista);

// 2.7 Elimina todos los nodos que tengan la clase .fn-remove-me
const borrar = document.querySelectorAll('.fn-remove-me');
for (const nodos of borrar) {
   nodos.remove();
}

// 2.8 Inserta una p con el texto 'Voy en medio!' entre los dos div. 
// 	Recuerda que no solo puedes insertar elementos con .appendChild.
const newText = document.createElement('p');
const ubicacionP = document.querySelectorAll('div');

newText.textContent = "Voy en medio";

document.body.insertBefore(newText, ubicacionP[1]);

// 2.9 Inserta p con el texto 'Voy dentro!', dentro de todos los div con la clase fn-insert-here
const clasesInsert = document.querySelectorAll('.fn-insert-here');

for (const unaClase of clasesInsert){
   var nuevaPe = document.createElement('p');
   nuevaPe.textContent = "Voy dentro!";
   unaClase.appendChild(nuevaPe);
}
